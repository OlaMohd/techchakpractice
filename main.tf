#configure the aws provider
provider "aws" {
  region = "us-east-2"
  default_tags {

    tags = {
      Application = "Cardealership3"
      owner       = "tchakgroup3"

    }
  }

}

## WEBSITE BUCKET START
resource "aws_s3_bucket" "moh1_practicewebsite_bucket" {
  bucket = var.moh1_practicewebsite_bucket

}

locals {
  s3_origin_id = "myS3Origin"
}

resource "aws_s3_bucket_website_configuration" "user_site" {

  bucket = aws_s3_bucket.moh1_practicewebsite_bucket.id
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "error.html"
  }
}


resource "aws_cloudfront_origin_access_control" "Group3techck" {
  name                              = "group3_access_control"
  description                       = "group3 Policy"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}
resource "aws_cloudfront_distribution" "mohcfront1" {
  origin {
    domain_name              = aws_s3_bucket.moh1_practicewebsite_bucket.bucket_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.Group3techck.id
    origin_id                = local.s3_origin_id
  }
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "team3"
  default_root_object = "index.html"
  # logging_config {
  #   include_cookies = false
  #   bucket          = "mylogs.s3.amazonaws.com"
  #   prefix          = "myprefix"
  # }
  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/content/immutable/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      headers      = ["Origin"]
      cookies {
        forward = "none"
      }
    }
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }
  # Cache behavior with precedence 1
  ordered_cache_behavior {
    path_pattern     = "/content/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }
  price_class = "PriceClass_200"
  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }
  tags = {
    Environment = "dev"
  }

  # Add an alternate domain name
  aliases = [
    "boykeme.com"
  ]
  # Use an existing SSL/TLS certificate
  viewer_certificate {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:126552587903:certificate/e6883f8f-fae6-41e9-b6ce-27360ad2ae3a"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }

}

variable "moh1_practicewebsite_bucket" {
  type        = string
  description = "S3 bucket storing static files for website"
  default     = "mohtfbucket1"
}

variable "bucket_acl" {
  type        = string
  default     = "private"
  description = "Bucket ACL (Access Control Listing)"
}

variable "domain_name" {
  type        = string
  default     = "aws_s3_bucket.mohtfbucket1.s3.us-east-1.amazonaws.com"
  description = "website name"
}


